package validator

import (
	"github.com/go-pg/pg/v10"
	"github.com/go-playground/validator/v10"
	"gokanji/db"
	"reflect"
	"strings"
)

type CustomValidator struct {
	validator *validator.Validate
}

func (v *CustomValidator) Validate(i interface{}) error {
	return v.validator.Struct(i)
}

// ValidateVar for validate field against tag. Expl: ValidateVar("abc@gmail.com", "required,email")
func (v *CustomValidator) ValidateVar(field interface{}, tag string) error {
	return v.validator.Var(field, tag)
}

func NewValidator() *CustomValidator {
	validate := validator.New()

	validate.RegisterTagNameFunc(func(fld reflect.StructField) string {
		name := strings.SplitN(fld.Tag.Get("json"), ",", 2)[0]
		if name == "-" {
			return ""
		}
		return name
	})

	validate.RegisterValidation("notexists", notExistsOnDbTable)

	return &CustomValidator{
		validator: validate,
	}
}

func notExistsOnDbTable(fl validator.FieldLevel) bool {
	var paramOne reflect.Value
	params := strings.Fields(fl.Param())
	parentVal := fl.Parent()

	if len(params) > 1 {
		if parentVal.Kind() == reflect.Ptr {
			parentVal = reflect.Indirect(parentVal)
		}
		paramOne = parentVal.FieldByName(params[1]) // normally is KecamatanID
		if paramOne.IsZero() {
			return false
		}
	}

	switch params[0] {
	case "email":
		userName := strings.TrimSpace(fl.Field().String())
		var cnt int
		db.PublicDB().QueryOne(pg.Scan(&cnt), `
			SELECT count(*)
			FROM public.users
			WHERE email = ?`, userName)
		return cnt == 0
	case "kanji":
		kanji := strings.TrimSpace(fl.Field().String())
		var cnt int
		db.PublicDB().QueryOne(pg.Scan(&cnt), `
			SELECT count(*)
			FROM public.kanjis
			WHERE kanji = ?`, kanji)
		return cnt == 0
	case "word":
		word := strings.TrimSpace(fl.Field().String())
		var cnt int
		db.PublicDB().QueryOne(pg.Scan(&cnt), `
			SELECT count(*)
			FROM public.words
			WHERE word = ?`, word)
		return cnt == 0
	}

	return false
}
