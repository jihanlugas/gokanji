//nolint
//lint:file-ignore U1000 ignore unused code, it's generated
package model

import (
	"time"
)

type PublicUserkanji struct {
	tableName struct{} `pg:"public.userkanjis,alias:userkanjis,,discard_unknown_columns"`

	UserkanjiID int64 `pg:"userkanji_id,pk" json:"userkanjiId" validate:"required"`
	UserID      int64 `pg:"user_id,use_zero" json:"userId" validate:"required"`
	KanjiID     int64 `pg:"kanji_id,use_zero" json:"kanjiId" validate:"required"`
}

func (m *PublicUserkanji) Name() string {
	return "PublicUserkanji"
}

func (m *PublicUserkanji) BeforeInsert(u int64, now *time.Time) {

}

func (m *PublicUserkanji) BeforeUpdate(u int64, now *time.Time) {

}

func (m *PublicUserkanji) BeforeArchive(u int64, now *time.Time) {

}
