//nolint
//lint:file-ignore U1000 ignore unused code, it's generated
package model

import (
	"time"
)

type PublicSentence struct {
	tableName struct{} `pg:"public.sentences,alias:sentences,,discard_unknown_columns"`

	SentenceID  int64      `pg:"sentence_id,pk" json:"sentenceId" validate:"required"`
	Sentence    string     `pg:"sentence,use_zero" json:"sentence" validate:"required"`
	Kana        string     `pg:"kana,use_zero" json:"kana" validate:"required"`
	Mean        string     `pg:"mean,use_zero" json:"mean" validate:"required"`
	Kanjis      []string   `pg:"kanjis,array" json:"kanjis"`
	Description string     `pg:"description" json:"description"`
	UserID      int64      `pg:"user_id,use_zero" json:"userId" validate:"required"`
	CreatedAt   *time.Time `pg:"created_at,use_zero" json:"createdAt" validate:"required"`
	UpdatedAt   *time.Time `pg:"updated_at,use_zero" json:"updatedAt" validate:"required"`
	CreatedBy   int64      `pg:"created_by,use_zero" json:"createdBy" validate:"required"`
	UpdatedBy   int64      `pg:"updated_by,use_zero" json:"updatedBy" validate:"required"`
}

func (m *PublicSentence) Name() string {
	return "PublicSentence"
}

func (m *PublicSentence) BeforeInsert(u int64, now *time.Time) {
	m.CreatedBy = u
	m.UpdatedBy = u
	m.CreatedAt = now
	m.UpdatedAt = now
}

func (m *PublicSentence) BeforeUpdate(u int64, now *time.Time) {
	m.UpdatedBy = u
	m.UpdatedAt = now
}

func (m *PublicSentence) BeforeArchive(u int64, now *time.Time) {

}
