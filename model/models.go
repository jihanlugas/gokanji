package model

import (
	jsoniter "github.com/json-iterator/go"
	"math/big"
	"time"
)

var json jsoniter.API
var percentFloat *big.Float
var tenthoFloat *big.Float
var millionFloat *big.Float
var deltaPlus *big.Float
var deltaMinus *big.Float

func init() {
	json = jsoniter.ConfigFastest
	percentFloat = new(big.Float).SetInt64(100)
	tenthoFloat = new(big.Float).SetInt64(10000)
	millionFloat = new(big.Float).SetInt64(1000000)
	deltaPlus = new(big.Float).SetFloat64(0.5)
	deltaMinus = new(big.Float).SetFloat64(-0.5)
}

type Model interface {
	Name() string
	BeforeInsert(u int64, now *time.Time)
	BeforeUpdate(u int64, now *time.Time)
	BeforeArchive(u int64, now *time.Time)
}
