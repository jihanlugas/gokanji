//nolint
//lint:file-ignore U1000 ignore unused code, it's generated
package model

import (
	"io/ioutil"
	"net/http"
	"time"
)

type PublicKanji struct {
	tableName struct{} `pg:"public.kanjis,alias:kanjis,,discard_unknown_columns"`

	KanjiID      int64      `pg:"kanji_id,pk" json:"kanjiId" validate:"required"`
	Kanji        string     `pg:"kanji,use_zero" json:"kanji" validate:"required"`
	Grade        int        `pg:"grade" json:"grade"`
	StrokeCount  int        `pg:"stroke_count" json:"strokeCount"`
	Meanings     []string   `pg:"meanings,array,use_zero" json:"meanings" validate:"required"`
	KunReadings  []string   `pg:"kun_readings,array,use_zero" json:"kunReadings" validate:"required"`
	OnReadings   []string   `pg:"on_readings,array,use_zero" json:"onReadings" validate:"required"`
	NameReadings []string   `pg:"name_readings,array,use_zero" json:"nameReadings" validate:"required"`
	Jlpt         int        `pg:"jlpt" json:"jlpt"`
	Unicode      string     `pg:"unicode" json:"unicode"`
	HeisigEn     string     `pg:"heisig_en" json:"heisigEn"`
	CreatedAt    *time.Time `pg:"created_at,use_zero" json:"createdAt" validate:"required"`
	UpdatedAt    *time.Time `pg:"updated_at,use_zero" json:"updatedAt" validate:"required"`
	CreatedBy    int64      `pg:"created_by,use_zero" json:"createdBy" validate:"required"`
	UpdatedBy    int64      `pg:"updated_by,use_zero" json:"updatedBy" validate:"required"`
}

type fetchKanji struct {
	Kanji        string     `json:"kanji"`
	Grade        int        `json:"grade"`
	StrokeCount  int        `json:"stroke_count"`
	Meanings     []string   `json:"meanings"`
	KunReadings  []string   `json:"kun_readings"`
	OnReadings   []string   `json:"on_readings"`
	NameReadings []string   `json:"name_readings"`
	Jlpt         int        `json:"jlpt"`
	Unicode      string     `json:"unicode"`
	HeisigEn     string     `json:"heisig_en"`
}

func (m *PublicKanji) Name() string {
	return "PublicKanji"
}

func (m *PublicKanji) BeforeInsert(u int64, now *time.Time) {
	m.CreatedBy = u
	m.UpdatedBy = u
	m.CreatedAt = now
	m.UpdatedAt = now
}

func (m *PublicKanji) BeforeUpdate(u int64, now *time.Time) {
	m.UpdatedBy = u
	m.UpdatedAt = now
}

func (m *PublicKanji) BeforeArchive(u int64, now *time.Time) {

}

func (m *PublicKanji) FetchKanji(kanji string) error {
	var err error
	url := "https://kanjiapi.dev/v1/kanji/" + kanji
	spaceClient := http.Client{
		Timeout: time.Second * 10, // Timeout after 2 seconds
	}
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return err
	}
	req.Header.Set("Accept", "application/json")
	res, err := spaceClient.Do(req)
	if err != nil {
		return err
	}
	if res.Body != nil {
		defer res.Body.Close()
	}
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return err
	}

	newKanji := fetchKanji{}
	err = json.Unmarshal(body, &newKanji)
	if err != nil {
		return err
	}

	m.Kanji = newKanji.Kanji
	m.Grade = newKanji.Grade
	m.StrokeCount = newKanji.StrokeCount
	m.Meanings = newKanji.Meanings
	m.KunReadings = newKanji.KunReadings
	m.OnReadings = newKanji.OnReadings
	m.NameReadings = newKanji.NameReadings
	m.Jlpt = newKanji.Jlpt
	m.Unicode = newKanji.Unicode
	m.HeisigEn = newKanji.HeisigEn

	return nil
}
