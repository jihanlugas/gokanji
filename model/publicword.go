//nolint
//lint:file-ignore U1000 ignore unused code, it's generated
package model

import (
	"time"
)

type PublicWord struct {
	tableName struct{} `pg:"public.words,alias:words,,discard_unknown_columns"`

	WordID      int64      `pg:"word_id,pk" json:"wordId" validate:"required"`
	Word        string     `pg:"word,use_zero" json:"word" validate:"required"`
	Kana        string     `pg:"kana,use_zero" json:"kana" validate:"required"`
	Means       []string   `pg:"means,array,use_zero" json:"means" validate:"required"`
	Kanjis      []string   `pg:"kanjis,array" json:"kanjis"`
	Description string     `pg:"description" json:"description"`
	UserID      int64      `pg:"user_id,use_zero" json:"userId" validate:"required"`
	CreatedAt   *time.Time `pg:"created_at,use_zero" json:"createdAt" validate:"required"`
	UpdatedAt   *time.Time `pg:"updated_at,use_zero" json:"updatedAt" validate:"required"`
	CreatedBy   int64      `pg:"created_by,use_zero" json:"createdBy" validate:"required"`
	UpdatedBy   int64      `pg:"updated_by,use_zero" json:"updatedBy" validate:"required"`
}

func (m *PublicWord) Name() string {
	return "PublicWord"
}

func (m *PublicWord) BeforeInsert(u int64, now *time.Time) {
	m.CreatedBy = u
	m.UpdatedBy = u
	m.CreatedAt = now
	m.UpdatedAt = now
}

func (m *PublicWord) BeforeUpdate(u int64, now *time.Time) {
	m.UpdatedBy = u
	m.UpdatedAt = now
}

func (m *PublicWord) BeforeArchive(u int64, now *time.Time) {

}
