package log

import (
	"gokanji/config"
	"os"
	"sync"
	"time"
)

type fileLock struct {
	sync.Mutex          // 8
	f          *os.File // 8
	writeable  bool     // 1
}

func (fl *fileLock) Close() {
	fl.Lock()
	if fl.writeable {
		fl.f.Close()
	}
	fl.Unlock()
}

func (fl *fileLock) WriteByte(p []byte) {
	fl.Lock()
	if fl.writeable {
		fl.f.Write(p)
	}
	fl.Unlock()
}

func (fl *fileLock) WriteString(s string) {
	now := time.Now()
	fl.Lock()
	if fl.writeable {
		fl.f.Write(now.AppendFormat(make([]byte, 0), "2006-01-02 15:04:05.000000 "))
		fl.f.Write([]byte(s))
		fl.f.Write([]byte{'\n'})
	}
	fl.Unlock()
}

var (
	Sql       fileLock
	ChangeDay chan struct{}
)

func init() {
	ChangeDay = make(chan struct{})
}

func Run() {
	sqlLogFile := "sql_log."
	var err error
	now := time.Now()

	sqlLogPath := config.LogPath + "/" + sqlLogFile + now.Format("2006-01-02") + ".log"
	err = os.MkdirAll(config.LogPath, 0755)
	logDirWriteable := (err == nil)

	if logDirWriteable {
		Sql.f, err = os.OpenFile(sqlLogPath, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
		Sql.writeable = (err == nil)
		logDirWriteable = Sql.writeable
	}

	go func() {
		defer func() {
			Sql.Close()
		}()

		for {
			select {
			case <-ChangeDay:
				if logDirWriteable {
					now = time.Now()
					Sql.Close()
					sqlLogPath = config.LogPath + "/" + sqlLogFile + now.Format("2006-01-02") + ".log"
					Sql.f, err = os.OpenFile(sqlLogPath, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
					Sql.writeable = (err == nil)

					logDirWriteable = Sql.writeable
				}
			}
		}
	}()
}