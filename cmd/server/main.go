package main

import (
	"gokanji/config"
	"gokanji/log"
	"gokanji/router"
)


// @title Swagger Example API
// @version 1.0
// @description This is a sample server celler server.
func main() {
	log.Run()
	r := router.Init()
	if config.Environment != config.PRODUCTION {
		r.Logger.Fatal(r.StartTLS(":"+config.ListenTo.Port, config.CertificateFilePath, config.CertificateKeyFilePath))
	} else {
		r.Logger.Fatal(r.Start(":" + config.ListenTo.Port))
	}
}
