package main

import (
	"fmt"
	"gokanji/config"
	"io/ioutil"

	"github.com/go-pg/pg/v10"
)

func main() {
	var err error
	sqlContent, err := ioutil.ReadFile("./docs/db.sql")
	if err != nil {
		panic(err)
	}
	sql := string(sqlContent)

	db := pg.Connect(&pg.Options{
		User:     config.DBInfo.Username,
		Addr:     config.DBInfo.Host + ":" + config.DBInfo.Port,
		Database: config.DBInfo.DbName,
		Password: config.DBInfo.Password,
	})

	if db.Exec(sql); err != nil {
		panic(err)
	}
	fmt.Println("success")
}
