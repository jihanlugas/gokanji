package db

import (
	"context"
	"fmt"
	"github.com/go-pg/pg/v10"
	"github.com/go-pg/pg/v10/orm"
	"gokanji/config"
	"gokanji/log"
	"gokanji/model"
	"gokanji/response"
	"net/http"
	"os"
	"time"
)

var (
	publicDB *pg.DB
)

type Transaction struct {
	*pg.Tx
	UserId    int64
	now       *time.Time
	nameSpace string
	returning string
}

type debugHook struct{}

var _ pg.QueryHook = (*debugHook)(nil)

func (debugHook) BeforeQuery(ctx context.Context, evt *pg.QueryEvent) (context.Context, error) {
	q, err := evt.FormattedQuery()

	if err != nil {
		log.Sql.WriteString(err.Error())
	} else if config.Environment != config.PRODUCTION {
		fmt.Println(string(q))
	}

	if evt.Err != nil {
		log.Sql.WriteString(evt.Err.Error())
	}

	return ctx, nil
}

func (debugHook) AfterQuery(c context.Context, evt *pg.QueryEvent) error {
	if evt.Err != nil {
		_, err := evt.FormattedQuery()

		if err != nil {
			log.Sql.WriteString(err.Error())
		} else if config.Environment != config.PRODUCTION {
			// fmt.Println(string(q))
		}
	}
	return nil
}

func init() {
	var onConnect func(context.Context, *pg.Conn) error = func(ctx context.Context, conn *pg.Conn) (err error) {
		_, err = conn.Exec("set timezone to 'UTC'")
		if err != nil {
			log.Sql.WriteString(err.Error())
			panic(err)
		}
		return err
	}

	publicDB = pg.Connect(&pg.Options{
		User:      config.DBInfo.Username,
		Addr:      config.DBInfo.Host + ":" + config.DBInfo.Port,
		Database:  config.DBInfo.DbName,
		Password:  config.DBInfo.Password,
		OnConnect: onConnect,
	})
	publicDB.AddQueryHook(debugHook{})

	if _, err := publicDB.Exec("SELECT 1"); err != nil {
		log.Sql.WriteString("db.init: " + err.Error())
		os.Exit(http.StatusInternalServerError)
	}
}

func PublicNextSeq(transaction *Transaction, sequenceName string, nextval *int8) {
	_, err := transaction.Query(pg.Scan(nextval), "select nextval('"+sequenceName+"')")
	if err != nil {
		panic(err)
	}
}

func PublicDB() *pg.DB {
	return publicDB
}

func PublicModel(model ...interface{}) *orm.Query {
	return publicDB.Model(model...)
}

func PublicBegin(userId int64, now *time.Time, nameSpace string) *Transaction {
	tx, e := publicDB.Begin()
	if e != nil {
		panic(response.ErrorSql("db.PublicBegin", e))
	}
	return &Transaction{tx, userId, now, nameSpace, ""}
}


func (t *Transaction) Insert(model model.Model) {
	model.BeforeInsert(t.UserId, t.now)
	q := t.Model(model)
	if t.returning != "" {
		q.Returning(t.returning)
	}
	if _, err := q.Insert(); err != nil {
		panic(response.ErrorSql(t.nameSpace+".insert."+model.Name(), err))
	}
}

func (t *Transaction) Update(model model.Model) {
	model.BeforeUpdate(t.UserId, t.now)
	q := t.Model(model)
	if t.returning != "" {
		q.Returning(t.returning)
	}
	if _, err := q.WherePK().Update(); err != nil {
		panic(response.ErrorSql(t.nameSpace+".update."+model.Name(), err))
	}
}

func (t *Transaction) Archive(model model.Model) {
	model.BeforeArchive(t.UserId, t.now)
	q := t.Model(model)
	if t.returning != "" {
		q.Returning(t.returning)
	}
	if _, err := q.WherePK().Update(); err != nil {
		panic(response.ErrorSql(t.nameSpace+".archive."+model.Name(), err))
	}
}

func (t *Transaction) Delete(model model.Model) {
	q := t.Model(model)
	if t.returning != "" {
		q.Returning(t.returning)
	}
	if _, err := q.WherePK().Delete(); err != nil {
		panic(response.ErrorSql(t.nameSpace+".delete."+model.Name(), err))
	}
}

func (t *Transaction) DeleteWhere(model model.Model, condition string, params ...interface{}) {
	q := t.Model(model)
	if t.returning != "" {
		q.Returning(t.returning)
	}
	if _, err := q.Where(condition, params...).Delete(); err != nil {
		panic(response.ErrorSql(t.nameSpace+".delete."+model.Name(), err))
	}
}

func HandleSelectError(err error, nameSpace string) {
	if err != nil && err.Error() != "pg: no rows in result set" {
		panic(response.ErrorSql(nameSpace, err))
	}
}

func IsNoRows(err error, nameSpace string) bool {
	if err == nil {
		return false
	} else if err.Error() == "pg: no rows in result set" {
		return true
	} else {
		panic(response.ErrorSql(nameSpace, err))
	}
}

func DeferHandleTransaction(transactions ...*Transaction) {
	if err := recover(); err != nil {
		for _, transaction := range transactions {
			transaction.Rollback()
			transaction.Close()
		}
		panic(err)
	}
}