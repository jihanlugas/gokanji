package router

import (
	"encoding/binary"
	"encoding/json"
	"fmt"
	"github.com/labstack/echo/v4"
	echoSwagger "github.com/swaggo/echo-swagger"
	"gokanji/config"
	"gokanji/constant"
	"gokanji/controller"
	"gokanji/cryption"
	"gokanji/response"
	"gokanji/swg"
	"net/http"
	"strconv"
	"time"
)

func init(){

}

type host struct {
	echo *echo.Echo
}

func Init() *echo.Echo {
	var err error
	var listenPort int
	hosts := make(map[string]*host)
	if listenPort, err = strconv.Atoi(config.ListenTo.Port); err != nil {
		panic(err)
	}

	web := websiteRouter()
	webDomain := config.WebDomainName
	hosts[webDomain] = &host{web}
	hosts[fmt.Sprintf("%s:%v", webDomain, listenPort)] = hosts[webDomain]

	//if config.Environment != config.PRODUCTION {
		web.GET("/", controller.Ping)
		web.GET("/movie", controller.Movie)
		web.GET("/swg/*", echoSwagger.WrapHandler)
	//}

	userController := controller.UserComposer()
	web.POST("/sign-in", userController.SignIn)
	web.GET("/sign-out", userController.SignOut)
	web.POST("/sign-up", userController.Create)
	//web.POST("/hash", userController.Hash)
	//web.POST("/cekhash", userController.Cekhash)

	checkToken := checkTokenMiddleware()

	webUser := web.Group("/user", checkToken)
	webUser.GET("/me", userController.Me)

	kanjiController := controller.KanjiComposer()
	webKanji := web.Group("/kanji", checkToken)
	webKanji.POST("/list", kanjiController.List)
	webKanji.POST("/form", kanjiController.Create)
	webKanji.GET("/:kanji_id", kanjiController.Get)
	//webKanji.GET("/search/:kanji", kanjiController.Search)
	webKanji.PUT("/update", kanjiController.Update)
	webKanji.DELETE("/:kanji_id", kanjiController.Delete)
	//webKanji.POST("/test", kanjiController.Test)

	userkanjiController := controller.UserkanjiComposer()
	webUserkanji := web.Group("/userkanji", checkToken)
	webUserkanji.POST("/list", userkanjiController.List)
	webUserkanji.POST("/search", userkanjiController.Search)


	sentenceController := controller.SentenceComposer()
	webSentence := web.Group("/sentence", checkToken)
	webSentence.POST("/list", sentenceController.List)
	webSentence.POST("/form", sentenceController.Create)
	webSentence.GET("/:sentence_id", sentenceController.Get)
	webSentence.PUT("/update", sentenceController.Update)
	webSentence.DELETE("/:sentence_id", sentenceController.Delete)

	wordController := controller.WordComposer()
	webWord := web.Group("/word", checkToken)
	webWord.POST("/list", wordController.List)
	webWord.POST("/form", wordController.Create)
	webWord.GET("/:word_id", wordController.Get)
	webWord.PUT("/update", wordController.Update)
	webWord.DELETE("/:word_id", wordController.Delete)

	e := echo.New()
	e.Any("/*", func(c echo.Context) (err error) {
		req := c.Request()
		res := c.Response()
		hst := hosts[req.Host]
		if config.Environment != config.PRODUCTION {
			swg.SwaggerInfo.Title = "Website API"
			swg.SwaggerInfo.BasePath = "/"
		}

		if hst == nil {
			err = echo.ErrNotFound
		} else {
			hst.echo.ServeHTTP(res, req)
		}

		return
	})
	return e
}


func checkTokenMiddleware() echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			cookie, err := c.Cookie(config.CookieAuthName)
			if err != nil {
				return response.ErrorForce("Akses ditolak!", response.Payload{}).SendJSON(c)
			}

			token := cookie.Value
			tokenPayload, err := cryption.DecryptAES64([]byte(token))
			if err != nil {
				return response.ErrorForce("Akses telah kadaluarsa", response.Payload{}).SendJSON(c)
			}

			if len(tokenPayload) == constant.TokenPayloadLen {
				expiredUnix := binary.BigEndian.Uint64(tokenPayload)
				expiredAt := time.Unix(int64(expiredUnix), 0)
				now := time.Now()
				if now.After(expiredAt) {
					return response.ErrorForce("Akses telah kadaluarsa!", response.Payload{}).SendJSON(c)
				} else {
					usrLogin := controller.UserLogin{
						UserID:      int64(binary.BigEndian.Uint64(tokenPayload[28:])),
					}
					c.Set(constant.TokenUserContext, usrLogin)
					return next(c)
				}
			} else {
				return response.ErrorForce("Akses telah kadaluarsa!", response.Payload{}).SendJSON(c)
			}
		}
	}
}

func httpErrorHandler(err error, c echo.Context) {
	var errorResponse *response.ErrorResponse
	code := http.StatusInternalServerError

	if err != nil {
		fmt.Println(err)
	}

	if he, ok := err.(*echo.HTTPError); ok {
		// Handle pada saat URL yang di request tidak ada. atau ada kesalahan server.
		code = he.Code
		errorResponse = &response.ErrorResponse{
			IsError: true,
			Message: strconv.Itoa(code) + " code. " + fmt.Sprintf("%v", he.Message),
			Payload: map[string]interface{}{},
		}
	} else if he, ok := err.(*response.ErrorResponse); ok {
		// Handle error dari kita sendiri
		errorResponse = he
	} else {
		// Handle error dari panic
		errorResponse = &response.ErrorResponse{
			IsError: true,
			Message: strconv.Itoa(code) + " code. Error Unresolved",
			Payload: map[string]interface{}{},
		}
		// log.Common.Error(err)
		// @todo ini harus tambahin log ke file untuk di trace lebih lanjut
	}

	js, err := json.Marshal(errorResponse)
	if err == nil {
		c.String(code, string(js))
	} else {
		b := []byte("{error: true, message: \"unresolved error\"}")
		c.Blob(code, echo.MIMEApplicationJSONCharsetUTF8, b)
	}
}