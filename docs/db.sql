CREATE TYPE userrole AS ENUM (
	'DEVELOPER',
	'SUPERADMIN',
	'ADMIN');


CREATE TABLE public.users (
    user_id bigserial NOT NULL,
    email varchar NOT NULL,
    fullname varchar NOT NULL,
    "password" varchar NOT NULL,
    created_at timestamptz NOT NULL,
    updated_at timestamptz NOT NULL,
    created_by int8 NOT NULL,
    updated_by int8 NOT NULL,
    active_dt timestamptz NULL,
    last_login timestamptz NULL
);

CREATE TABLE public.kanjis (
   kanji_id bigserial NOT NULL,
   kanji varchar NOT NULL,
   grade int2 NULL,
   stroke_count int2 NULL,
   meanings _varchar NOT NULL,
   kun_readings _varchar NOT NULL,
   on_readings _varchar NOT NULL,
   name_readings _varchar NOT NULL,
   jlpt int2 NULL,
   unicode varchar NULL,
   heisig_en varchar NULL,
   created_at timestamptz NOT NULL,
   updated_at timestamptz NOT NULL,
   created_by int8 NOT NULL,
   updated_by int8 NOT NULL,
   CONSTRAINT kanjis_pk PRIMARY KEY (kanji_id),
   CONSTRAINT kanjis_un UNIQUE (kanji)
);

CREATE TABLE public.sentences (
    sentence_id bigserial NOT NULL,
    sentence varchar NOT NULL,
    kana varchar NOT NULL,
    mean varchar NOT NULL,
    kanjis _varchar NULL,
    created_at timestamptz NOT NULL,
    updated_at timestamptz NOT NULL,
    created_by int8 NOT NULL,
    updated_by int8 NOT NULL,
    description varchar NULL,
    user_id int8 NOT NULL,
    CONSTRAINT sentences_pk PRIMARY KEY (sentence_id)
);

CREATE TABLE public.userkanjis (
   userkanji_id bigserial NOT NULL,
   user_id int8 NOT NULL,
   kanji_id int8 NOT NULL,
   CONSTRAINT userkanjis_pk PRIMARY KEY (userkanji_id)
);

CREATE TABLE public.words (
    word_id bigserial NOT NULL,
    word varchar NOT NULL,
    kana varchar NOT NULL,
    means _varchar NOT NULL,
    created_at timestamptz NOT NULL,
    updated_at timestamptz NOT NULL,
    created_by int8 NOT NULL,
    updated_by int8 NOT NULL,
    kanjis _varchar NULL,
    description varchar NULL,
    user_id int8 NOT NULL,
    CONSTRAINT words_pk PRIMARY KEY (word_id)
);




