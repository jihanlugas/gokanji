package helper

import (
	"regexp"
)

// UNICODE RANGE : DESCRIPTION
//
// 3000-303F : punctuation
// 3040-309F : hiragana
// 30A0-30FF : katakana
// FF00-FFEF : Full-width roman + half-width katakana
// 4E00-9FAF : Common and uncommon kanji
//
// Non-Japanese punctuation/formatting characters commonly used in Japanese text
// 2605-2606 : Stars
// 2190-2195 : Arrows
// u203B     : Weird asterisk thing

// regexp [^一-龯] === [\u4E00-\u9FAF]
func GetKanji(text string) (string) {
	reg, err := regexp.Compile("[^一-龯]+")
	if err != nil {
		return ""
	}
	return reg.ReplaceAllString(text, "")
}
