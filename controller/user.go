package controller

import (
	"github.com/labstack/echo/v4"
	"gokanji/config"
	"gokanji/cryption"
	"gokanji/db"
	"gokanji/model"
	"gokanji/response"
	"net/http"
	"strings"
	"time"
)

type User struct{}

func UserComposer() User {
	return User{}
}

type userLogin struct {
	Email    string `json:"email" validate:"required" example:"jihanlugas2@gmail.com"`
	Password string `json:"password" validate:"required" example:"123456"`
}

type userCreateReq struct {
	Email     string     `json:"email" validate:"required" example:"user@example.com"`
	Fullname  string     `json:"fullname" validate:"required" example:"User Name"`
	Password  string     `json:"password" validate:"required" example:"123456"`
}

type userRes struct {
	tableName struct{} `pg:"public.users,alias:users,,discard_unknown_columns"`

	UserID    int64      `pg:"user_id,pk" json:"userId"`
	Email     string     `pg:"email,use_zero" json:"email"`
	Fullname  string     `pg:"fullname,use_zero" json:"fullname"`
	Password  string     `pg:"password,use_zero" json:"-"`
}

type user struct {
	UserID    int64      `json:"userId"`
	Email     string     `json:"email"`
	Fullname  string     `json:"fullname"`
}

type hashReq struct {
	Password  string     `pg:"password,use_zero" json:"password"`
}

// @Tags Authentication
// @Summary Authentication sign-in
// @Accept json
// @Produce json
// @Param req body userLogin true "username and password pair"
// @Success 200 {object} response.SuccessResponse{payload=userRes} "json with success = true"
// @Failure 400 {object} response.ErrorResponse "json with error = true"
// @Router /sign-in [post]
func (h User) SignIn(c echo.Context) error {
	var err error
	req := new(userLogin)
	if err = c.Bind(req); err != nil {
		return err
	}
	if err = c.Validate(req); err != nil {
		return response.Error("Validation Failed", response.ValidationError(err)).SendJSON(c)
	}

	var qUser userRes
	err = db.PublicModel(&qUser).Where("email = ?", strings.ToLower(req.Email)).Select()
	if db.IsNoRows(err, c.Path()) {
		return response.Error("Nama pengguna atau kata sandi tidak valid", response.Payload{}).SendJSON(c)
	}

	match := cryption.CheckPasswordHash(req.Password, qUser.Password)
	if !match {
		return response.Error("Nama pengguna atau kata sandi tidak valid!", response.Payload{}).SendJSON(c)
	}

	now := time.Now()
	publicTransaction := db.PublicBegin(qUser.UserID, &now, c.Path())
	defer db.DeferHandleTransaction(publicTransaction)

	//qUser.LastLogin = &now
	//publicTransaction.Model(&qUser).
	//	Set("last_login = ?last_login").
	//	Where("user_id = ?user_id").
	//	Update()

	expiredAt := time.Now().AddDate(1, 0, 0)
	token, err := getLoginToken(int64(qUser.UserID), expiredAt)
	if err != nil {
		return response.Error("Nama pengguna atau kata sandi tidak valid.", response.Payload{}).SendJSON(c)
	}

	maxAge := expiredAt.Sub(now).Seconds()
	cookie := new(http.Cookie)
	cookie.Name = config.CookieAuthName
	cookie.Value = string(token)
	cookie.Path = "/"
	cookie.Expires = expiredAt
	cookie.MaxAge = int(maxAge)


	if config.Environment == config.PRODUCTION {
		cookie.SameSite = http.SameSiteNoneMode
		cookie.HttpOnly = true
		cookie.Secure = true
	} else {
		cookie.SameSite = http.SameSiteNoneMode
		cookie.HttpOnly = true
		cookie.Secure = true
	}

	c.SetCookie(cookie)
	publicTransaction.Commit()

	return response.Success("Login Success ", qUser).SendJSON(c)
}


// @Tags Authentication
// @Summary Authentication sign-out
// @Accept json
// @Produce json
// @Success 200 {object} response.SuccessResponse "json with success = true"
// @Failure 400 {object} response.ErrorResponse "json with error = true"
// @Router /sign-out [get]
func (h User) SignOut(c echo.Context) error {
	expiresAt := time.Now().Add(-100 * time.Hour)
	now := time.Now()
	maxAge := expiresAt.Sub(now).Seconds()

	cookie := new(http.Cookie)
	cookie.Name = "Authorization"
	cookie.Value = ""
	cookie.Path = "/"
	cookie.Expires = expiresAt
	cookie.MaxAge = int(maxAge)

	if config.Environment == config.PRODUCTION {
		cookie.SameSite = http.SameSiteNoneMode
		cookie.HttpOnly = true
		cookie.Secure = true
	} else {
		cookie.SameSite = http.SameSiteNoneMode
		cookie.HttpOnly = true
		cookie.Secure = true
	}
	c.SetCookie(cookie)
	return response.Success("Logout Success!", response.Payload{}).SendJSON(c)
}

// @Tags User
// @Summary Get current login user
// @Accept json
// @Produce json
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
// @Success 200 {object} response.SuccessResponse{payload=user} "json with success = true"
// @Failure 400 {object} response.ErrorResponse "json with error = true"
// @Router /user/me [get]
func (h User) Me(c echo.Context) error {
	var err error
	loginUser, err := getUserLoginInfo(c)
	if err != nil {
		return err
	}

	var form model.PublicUser
	form.UserID = loginUser.UserID
	err = db.PublicDB().Model(&form).WherePK().Select()
	if db.IsNoRows(err, c.Path()) {
		return response.Error("Not Found", response.Payload{}).SendJSON(c)
	}

	u := user{
		UserID:   form.UserID,
		Email:    form.Email,
		Fullname: form.Fullname,
	}

	return response.Success("Success", u).SendJSON(c)
}



// @Tags Authentication
// @Summary Sign-up
// @Accept json
// @Produce json
// @Param req body userCreateReq true "json request body"
// @Success 200 {object} response.SuccessResponse{payload=userRes} "json with success = true"
// @Failure 400 {object} response.ErrorResponse "json with error = true"
// @Router /sign-up [post]
func (h User) Create(c echo.Context) error {
	var err error
	now := time.Now()
	req := new(userCreateReq)
	if err = c.Bind(req); err != nil {
		return err
	}

	if err = c.Validate(req); err != nil {
		return response.Error("Validation Failed", response.ValidationError(err)).SendJSON(c)
	}

	hash, err := cryption.HashPassword(req.Password)
	if err != nil {
		return response.Error("Internal server error", response.Payload{}).SendJSON(c)
	}

	publicTransaction := db.PublicBegin(1, &now, c.Path())
	defer db.DeferHandleTransaction(publicTransaction)

	newUser := model.PublicUser{
		Email:     req.Email,
		Fullname:  req.Fullname,
		Password:  hash,
		ActiveDt:  &now,
		LastLogin: &now,
	}
	publicTransaction.Insert(&newUser)
	publicTransaction.Commit()

	user := userRes{
		UserID:    newUser.UserID,
		Email:     newUser.Email,
		Fullname:  newUser.Fullname,
	}

	return response.Error("Success create user", user).SendJSON(c)
}

func (h User) Hash(c echo.Context) error {
	var err error
	req := new(hashReq)
	if err = c.Bind(req); err != nil {
		return err
	}

	hash, err := cryption.HashPassword(req.Password)

	return response.Success("Not Implement " + hash, response.Payload{}).SendJSON(c)
}

//// @Tags Authentication
//// @Summary Hashpassword
//// @Accept json
//// @Produce json
//// @Param req body hashReq true "json request body"
//// @Success 200 {object} response.SuccessResponse "json with success = true"
//// @Failure 400 {object} response.ErrorResponse "json with error = true"
//// @Router /cekhash [post]
//func (h User) Cekhash(c echo.Context) error {
//	var err error
//	req := new(hashReq)
//	if err = c.Bind(req); err != nil {
//		return err
//	}
//
//	match := cryption.CheckPasswordHash(req.Password, "$2a$15$BxjfRWco5.LsQS1ekgdyXeaTkzpJlV4NiF6yom3vq7YYP40x9EQhe")
//	if match {
//		return response.Success("True ", response.Payload{}).SendJSON(c)
//	} else {
//		return response.Success("False ", response.Payload{}).SendJSON(c)
//	}
//
//	return response.Success("Not Implement ", response.Payload{}).SendJSON(c)
//}

