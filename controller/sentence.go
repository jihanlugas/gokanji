package controller

import (
	"github.com/go-pg/pg/v10"
	"github.com/labstack/echo/v4"
	"gokanji/db"
	"gokanji/helper"
	"gokanji/model"
	"gokanji/response"
	"gokanji/sql"
	"strconv"
	"time"
)

type Sentence struct{}

func SentenceComposer() Sentence {
	return Sentence{}
}

type sentenceListReq struct {
	Sentence    string `json:"sentence" example:" "`
	Kana        string `json:"kana" example:" "`
	Mean        string `json:"mean" example:" "`
	Description string `json:"description" example:" "`
}

type sentence struct {
	tableName struct{} `pg:"public.sentences,alias:sentences,,discard_unknown_columns"`

	SentenceID  int64    `pg:"sentence_id,pk" json:"sentenceId"`
	Sentence    string   `pg:"sentence,use_zero" json:"sentence" example:" "`
	Kana        string   `pg:"kana,use_zero" json:"kana" example:" "`
	Mean        string   `pg:"mean,use_zero" json:"mean" example:" "`
	Kanjis      []string `pg:"kanjis,array" json:"kanjis" example:" "`
	Description string   `pg:"description" json:"description" example:" "`
}

type sentenceCreateReq struct {
	Sentence    string `json:"sentence" example:"" validate:"required"`
	Kana        string `json:"kana" example:" " validate:"required"`
	Mean        string `json:"mean" example:" " validate:"required"`
	Description string `json:"description"`
}

type sentenceUpdateReq struct {
	SentenceID  int64  `json:"sentenceId" validate:"required"`
	Sentence    string `json:"sentence" example:"" validate:"required"`
	Kana        string `json:"kana" example:" " validate:"required"`
	Mean        string `json:"mean" example:" " validate:"required"`
	Description string `json:"description"`
}

// @Summary Sentence Create
// @Tags Sentence
// @Accept json
// @Produce json
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
// @Param req body sentenceCreateReq false "json request body"
// @Success 200 {object} response.SuccessResponse{payload=sentence} "json with success = true"
// @Failure 400 {object} response.ErrorResponse "json with error = true"
// @Router /sentence/form [post]
func (h Sentence) Create(c echo.Context) error {
	var err error
	var found bool
	loginUser, err := getUserLoginInfo(c)
	if err != nil {
		return err
	}
	now := time.Now()
	req := new(sentenceCreateReq)
	if err = c.Bind(req); err != nil {
		return err
	}
	if err = c.Validate(req); err != nil {
		return response.Error("Validation Failed", response.ValidationError(err)).SendJSON(c)
	}

	// begin Transaction Public
	publicTransaction := db.PublicBegin(loginUser.UserID, &now, c.Path())
	defer db.DeferHandleTransaction(publicTransaction)

	sentencekanji := helper.GetKanji(req.Sentence)
	kanjis := []string{}
	for i := 0; i < len([]rune(sentencekanji)); i++ {
		k := string([]rune(sentencekanji)[i])
		found = false
		for _, kanji := range kanjis {
			if kanji == k {
				found = true
				break
			}
		}
		if !found {
			kanjis = append(kanjis, k)
		}
	}

	var listKanji []model.PublicKanji
	err = db.PublicDB().Model(&listKanji).Where("kanjis.kanji IN (?) ", pg.In(kanjis)).Select()
	if db.IsNoRows(err, c.Path()) || len(listKanji) == 0 {
		listKanji = make([]model.PublicKanji, 0)
	}

	for _, newkanji := range kanjis {
		kanji := model.PublicKanji{}
		userkanji := model.PublicUserkanji{}
		kanjiexist := false
		for _, list := range listKanji {
			if list.Kanji == newkanji {
				kanjiexist = true
				kanji = list
				break
			}
		}

		if !kanjiexist {
			if err = kanji.FetchKanji(newkanji); err == nil {
				publicTransaction.Insert(&kanji)
			}
			userkanji.UserID = loginUser.UserID
			userkanji.KanjiID = kanji.KanjiID
			publicTransaction.Insert(&userkanji)
		} else {
			err = db.PublicDB().Model(&userkanji).
				Where("userkanjis.user_id = ? ", loginUser.UserID).
				Where("userkanjis.kanji_id = ? ", kanji.KanjiID).
				First()
			if db.IsNoRows(err, c.Path()) {
				userkanji.UserID = loginUser.UserID
				userkanji.KanjiID = kanji.KanjiID
				publicTransaction.Insert(&userkanji)
			}
		}
	}

	newSentence := model.PublicSentence{
		Sentence:    req.Sentence,
		Kana:        req.Kana,
		Mean:        req.Mean,
		Kanjis:      kanjis,
		Description: req.Description,
		UserID:      loginUser.UserID,
	}
	publicTransaction.Insert(&newSentence)
	publicTransaction.Commit()

	sentence := sentence{
		SentenceID:  newSentence.SentenceID,
		Sentence:    newSentence.Sentence,
		Kana:        newSentence.Kana,
		Mean:        newSentence.Mean,
		Kanjis:      newSentence.Kanjis,
		Description: newSentence.Description,
	}

	return response.Success("Sentence Created", sentence).SendJSON(c)
}

// @Summary Get sentence
// @Tags Sentence
// @Accept json
// @Produce json
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
// @Param sentence_id path string true "sentence id" default(0)
// @Success 200 {object} response.SuccessResponse{payload=sentence} "json with success = true"
// @Failure 400 {object} response.ErrorResponse "json with error = true"
// @Router /sentence/{sentence_id} [get]
func (h Sentence) Get(c echo.Context) error {
	var err error
	loginUser, err := getUserLoginInfo(c)
	if err != nil {
		return err
	}
	reqID, err := strconv.ParseInt(c.Param("sentence_id"), 10, 64)
	if err != nil {
		return err
	}

	var form sentence
	form.SentenceID = reqID
	if form.SentenceID == 0 {
		return response.Error("ID required", response.Payload{}).SendJSON(c)
	} else {
		err = db.PublicDB().Model(&form).WherePK().Where("sentences.user_id = ? ", loginUser.UserID).Select()
		if err != nil {
			return response.Error("Not Found", response.Payload{}).SendJSON(c)
		}
	}
	return response.Success("Success get sentence", form).SendJSON(c)
}

// @Summary Update sentence
// @Tags Sentence
// @Accept json
// @Produce json
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
// @Param req body sentenceUpdateReq true "json request body"
// @Success 200 {object} response.SuccessResponse{payload=sentence} "json with success = true"
// @Failure 400 {object} response.ErrorResponse "json with error = true"
// @Router /sentence/update [put]
func (h Sentence) Update(c echo.Context) error {
	var err error
	var found bool
	loginUser, err := getUserLoginInfo(c)
	if err != nil {
		return err
	}
	now := time.Now()
	req := new(sentenceUpdateReq)
	if err = c.Bind(req); err != nil {
		return err
	}
	if err = c.Validate(req); err != nil {
		return response.Error("Validation Failed", response.ValidationError(err)).SendJSON(c)
	}
	if req.SentenceID == 0 {
		return response.Error("ID Required", response.Payload{}).SendJSON(c)
	}

	qSentence := model.PublicSentence{SentenceID: req.SentenceID}
	err = db.PublicDB().Model(&qSentence).WherePK().Select()
	if db.IsNoRows(err, c.Path()) {
		return response.Error("Not Found", response.Payload{}).SendJSON(c)
	}

	publicTransaction := db.PublicBegin(loginUser.UserID, &now, c.Path())
	defer db.DeferHandleTransaction(publicTransaction)

	sentencekanji := helper.GetKanji(req.Sentence)
	kanjis := []string{}
	for i := 0; i < len([]rune(sentencekanji)); i++ {
		k := string([]rune(sentencekanji)[i])
		found = false
		for _, kanji := range kanjis {
			if kanji == k {
				found = true
				break
			}
		}
		if !found {
			kanjis = append(kanjis, k)
		}
	}

	var listKanji []model.PublicKanji
	err = db.PublicDB().Model(&listKanji).Where("kanjis.kanji IN (?) ", pg.In(kanjis)).Select()
	if db.IsNoRows(err, c.Path()) || len(listKanji) == 0 {
		listKanji = make([]model.PublicKanji, 0)
	}

	for _, newkanji := range kanjis {
		kanji := model.PublicKanji{}
		userkanji := model.PublicUserkanji{}
		kanjiexist := false
		for _, list := range listKanji {
			if list.Kanji == newkanji {
				kanjiexist = true
				kanji = list
				break
			}
		}

		if !kanjiexist {
			if err = kanji.FetchKanji(newkanji); err == nil {
				publicTransaction.Insert(&kanji)
			}
			userkanji.UserID = loginUser.UserID
			userkanji.KanjiID = kanji.KanjiID
			publicTransaction.Insert(&userkanji)
		} else {
			err = db.PublicDB().Model(&userkanji).
				Where("userkanjis.user_id = ? ", loginUser.UserID).
				Where("userkanjis.kanji_id = ? ", kanji.KanjiID).
				First()
			if db.IsNoRows(err, c.Path()) {
				userkanji.UserID = loginUser.UserID
				userkanji.KanjiID = kanji.KanjiID
				publicTransaction.Insert(&userkanji)
			}
		}
	}

	qSentence.Sentence = req.Sentence
	qSentence.Kana = req.Kana
	qSentence.Mean = req.Mean
	qSentence.Kanjis = kanjis
	qSentence.Description = req.Description
	qSentence.UserID = loginUser.UserID

	publicTransaction.Update(&qSentence)
	publicTransaction.Commit()

	sentence := sentence{
		SentenceID:  qSentence.SentenceID,
		Sentence:    qSentence.Sentence,
		Kana:        qSentence.Kana,
		Mean:        qSentence.Mean,
		Kanjis:      qSentence.Kanjis,
		Description: qSentence.Description,
	}
	return response.Success("Sentence Edited", sentence).SendJSON(c)
}

// @Summary Delete sentence
// @Tags Sentence
// @Accept json
// @Produce json
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
// @Param sentence_id path string true "sentence id" default(0)
// @Success 200 {object} response.SuccessResponse "json with success = true"
// @Failure 400 {object} response.ErrorResponse "json with error = true"
// @Router /sentence/{sentence_id} [delete]
func (h Sentence) Delete(c echo.Context) error {
	var err error
	now := time.Now()
	loginUser, err := getUserLoginInfo(c)
	if err != nil {
		return err
	}
	reqID, err := strconv.ParseInt(c.Param("sentence_id"), 10, 64)
	if err != nil {
		return err
	}

	var form model.PublicSentence
	form.SentenceID = reqID
	if form.SentenceID == 0 {
		return response.Error("ID required", response.Payload{}).SendJSON(c)
	} else {
		err = db.PublicDB().Model(&form).WherePK().Select()
		if err != nil {
			return response.Error("Not Found", response.Payload{}).SendJSON(c)
		}
	}

	publicTransaction := db.PublicBegin(loginUser.UserID, &now, c.Path())
	defer db.DeferHandleTransaction(publicTransaction)

	publicTransaction.Delete(&form)
	publicTransaction.Commit()

	return response.Success("Sentence Deleted", response.Payload{}).SendJSON(c)
}

// @Summary Sentence List
// @Tags Sentence
// @Accept json
// @Produce json
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
// @Param req body sentenceListReq false "json request body"
// @Success 200 {object} response.SuccessResponse{payload=[]sentence} "json with success = true"
// @Failure 400 {object} response.ErrorResponse "json with error = true"
// @Router /sentence/list [post]
func (h Sentence) List(c echo.Context) error {
	var err error
	loginUser, err := getUserLoginInfo(c)
	if err != nil {
		return err
	}

	req := new(sentenceListReq)
	if err = c.Bind(req); err != nil {
		return err
	}
	qList := getListSentence(req, loginUser, c.Path())

	return response.Success("Get List Sentence Success", qList).SendJSON(c)
}

func getListSentence(req *sentenceListReq, loginUser UserLogin, nameSpace string) []sentence {
	var qList []sentence
	var err error

	q := db.PublicDB().Model(&qList).Where("sentences.user_id = ? ", loginUser.UserID)
	sql.Search.StringLike(q, "sentences.sentence", req.Sentence)
	sql.Search.StringLike(q, "sentences.kana", req.Kana)
	sql.Search.StringLike(q, "sentences.mean", req.Mean)
	sql.Search.StringLike(q, "sentences.description", req.Description)
	err = q.Select()
	if db.IsNoRows(err, nameSpace) || len(qList) == 0 {
		qList = make([]sentence, 0)
	}
	return qList
}
