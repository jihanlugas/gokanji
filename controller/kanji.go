package controller

import (
	"github.com/labstack/echo/v4"
	"gokanji/db"
	"gokanji/model"
	"gokanji/response"
	"gokanji/sql"
	"strconv"
	"time"
)

type Kanji struct{}

func KanjiComposer() Kanji {
	return Kanji{}
}

type kanjiListReq struct {
	Kanji    string `json:"kanji" example:" "`
	Unicode  string `json:"unicode" example:" "`
	HeisigEn string `json:"heisigEn" example:" "`
}

type kanji struct {
	tableName struct{} `pg:"public.kanjis,alias:kanjis,,discard_unknown_columns"`

	KanjiID      int64      `pg:"kanji_id,pk" json:"kanjiId"`
	Kanji        string     `pg:"kanji,use_zero" json:"kanji" example:"愛"`
	Grade        int        `pg:"grade" json:"grade" example:"4"`
	StrokeCount  int        `pg:"stroke_count" json:"strokeCount" example:"13"`
	Meanings     []string   `pg:"meanings,array,use_zero" json:"meanings" example:"love,affection,favourite"`
	KunReadings  []string   `pg:"kun_readings,array,use_zero" json:"kunReadings" example:"いと.しい,かな.しい,め.でる,お.しむ,まな"`
	OnReadings   []string   `pg:"on_readings,array,use_zero" json:"onReadings" example:"アイ"`
	NameReadings []string   `pg:"name_readings,array,use_zero" json:"nameReadings" example:"あ,あし,え,かな,なる,めぐ,めぐみ,よし,ちか"`
	Jlpt         int        `pg:"jlpt" json:"jlpt" example:"2"`
	Unicode      string     `pg:"unicode,use_zero" json:"unicode" example:"611b"`
	HeisigEn     string     `pg:"heisig_en" json:"heisigEn" example:"love"`
	ListWord     []word     `pg:"-" json:"listWord,omitempty"`
	ListSentence []sentence `pg:"-" json:"listSentence,omitempty"`
}

type kanjiCreateReq struct {
	Kanji        string   `pg:"kanji,use_zero" json:"kanji" example:"" validate:"required"`
	Grade        int      `pg:"grade" json:"grade" example:""`
	StrokeCount  int      `pg:"stroke_count" json:"strokeCount" example:""`
	Meanings     []string `pg:"meanings,array,use_zero" json:"meanings" example:"" validate:"required"`
	KunReadings  []string `pg:"kun_readings,array,use_zero" json:"kunReadings" example:"" validate:"required"`
	OnReadings   []string `pg:"on_readings,array,use_zero" json:"onReadings" example:"" validate:"required"`
	NameReadings []string `pg:"name_readings,array,use_zero" json:"nameReadings" example:"" validate:"required"`
	Jlpt         int      `pg:"jlpt" json:"jlpt" example:""`
	Unicode      string   `pg:"unicode,use_zero" json:"unicode" example:"" validate:"required"`
	HeisigEn     string   `pg:"heisig_en" json:"heisigEn" example:""`
}

type kanjiUpdateReq struct {
	KanjiID      int64    `pg:"kanji_id,pk" json:"kanjiId" validate:""required`
	Kanji        string   `pg:"kanji,use_zero" json:"kanji" example:"" validate:"required"`
	Grade        int      `pg:"grade" json:"grade" example:""`
	StrokeCount  int      `pg:"stroke_count" json:"strokeCount" example:""`
	Meanings     []string `pg:"meanings,array,use_zero" json:"meanings" example:"" validate:"required"`
	KunReadings  []string `pg:"kun_readings,array,use_zero" json:"kunReadings" example:"" validate:"required"`
	OnReadings   []string `pg:"on_readings,array,use_zero" json:"onReadings" example:"" validate:"required"`
	NameReadings []string `pg:"name_readings,array,use_zero" json:"nameReadings" example:"" validate:"required"`
	Jlpt         int      `pg:"jlpt" json:"jlpt" example:""`
	Unicode      string   `pg:"unicode,use_zero" json:"unicode" example:"" validate:"required"`
	HeisigEn     string   `pg:"heisig_en" json:"heisigEn" example:""`
}

// @Summary Kanji Create
// @Tags Kanji
// @Accept json
// @Produce json
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
// @Param req body kanjiCreateReq false "json request body"
// @Success 200 {object} response.SuccessResponse{payload=kanji} "json with success = true"
// @Failure 400 {object} response.ErrorResponse "json with error = true"
// @Router /kanji/form [post]
func (h Kanji) Create(c echo.Context) error {
	var err error
	loginUser, err := getUserLoginInfo(c)
	if err != nil {
		return err
	}
	now := time.Now()
	req := new(kanjiCreateReq)
	if err = c.Bind(req); err != nil {
		return err
	}
	if err = Validate.ValidateVar(req.Kanji, "notexists=kanji"); err != nil {
		return response.Error("Validation Failed", response.ValidationError(err)).SendJSON(c)
	}

	// begin Transaction Public
	publicTransaction := db.PublicBegin(loginUser.UserID, &now, c.Path())
	defer db.DeferHandleTransaction(publicTransaction)

	newKanji := model.PublicKanji{
		Kanji:        req.Kanji,
		Grade:        req.Grade,
		StrokeCount:  req.StrokeCount,
		Meanings:     req.Meanings,
		KunReadings:  req.KunReadings,
		OnReadings:   req.OnReadings,
		NameReadings: req.NameReadings,
		Jlpt:         req.Jlpt,
		Unicode:      req.Unicode,
		HeisigEn:     req.HeisigEn,
	}

	publicTransaction.Insert(&newKanji)
	publicTransaction.Commit()

	kanji := kanji{
		KanjiID:      newKanji.KanjiID,
		Kanji:        newKanji.Kanji,
		Grade:        newKanji.Grade,
		StrokeCount:  newKanji.StrokeCount,
		Meanings:     newKanji.Meanings,
		KunReadings:  newKanji.KunReadings,
		OnReadings:   newKanji.OnReadings,
		NameReadings: newKanji.NameReadings,
		Jlpt:         newKanji.Jlpt,
		Unicode:      newKanji.Unicode,
		HeisigEn:     newKanji.HeisigEn,
	}

	return response.Success("Kanji Created", kanji).SendJSON(c)
}

// @Summary Get kanji
// @Tags Kanji
// @Accept json
// @Produce json
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
// @Param kanji_id path string true "kanji id" default(0)
// @Success 200 {object} response.SuccessResponse{payload=kanji} "json with success = true"
// @Failure 400 {object} response.ErrorResponse "json with error = true"
// @Router /kanji/{kanji_id} [get]
func (h Kanji) Get(c echo.Context) error {
	var err error
	_, err = getUserLoginInfo(c)
	if err != nil {
		return err
	}
	reqID, err := strconv.ParseInt(c.Param("kanji_id"), 10, 64)
	if err != nil {
		return response.Error("Not Found", response.Payload{}).SendJSON(c)
	}

	var form kanji
	form.KanjiID = reqID
	if form.KanjiID == 0 {
		return response.Error("ID required", response.Payload{}).SendJSON(c)
	} else {
		err = db.PublicDB().Model(&form).WherePK().Select()
		if db.IsNoRows(err, c.Path()) {
			return response.Error("Not Found", response.Payload{}).SendJSON(c)
		}
	}

	var qWord []word
	err = db.PublicDB().Model(&qWord).Where("? = any (words.kanjis)", form.Kanji).Select()
	if db.IsNoRows(err, c.Path()) || len(qWord) == 0 {
		qWord = make([]word, 0)
	}
	form.ListWord = qWord

	var qSentence []sentence
	err = db.PublicDB().Model(&qSentence).Where("? = any (sentences.kanjis)", form.Kanji).Select()
	if db.IsNoRows(err, c.Path()) || len(qSentence) == 0 {
		qSentence = make([]sentence, 0)
	}
	form.ListSentence = qSentence

	return response.Success("Success get kanji", form).SendJSON(c)
}

//// @Summary Search kanji
//// @Tags Kanji
//// @Accept json
//// @Produce json
//// @securityDefinitions.apikey ApiKeyAuth
//// @in header
//// @name Authorization
//// @Param kanji path string true "kanji" default()
//// @Success 200 {object} response.SuccessResponse{payload=kanji} "json with success = true"
//// @Failure 400 {object} response.ErrorResponse "json with error = true"
//// @Router /kanji/search/{kanji} [get]
//func (h Kanji) Search(c echo.Context) error {
//	var err error
//	_, err = getUserLoginInfo(c)
//	if err != nil {
//		return err
//	}
//	reqKanji := c.Param("kanji")
//	if reqKanji == "" {
//		return response.Error("Not Found", response.Payload{}).SendJSON(c)
//	}
//
//	var form kanji
//	form.Kanji = reqKanji
//	if form.Kanji == "" {
//		return response.Error("Kanji required", response.Payload{}).SendJSON(c)
//	} else {
//		err = db.PublicDB().Model(&form).Where("kanjis.kanji = ? ", form.Kanji).First()
//		if db.IsNoRows(err, c.Path()) {
//			return response.Error("Not Found", response.Payload{}).SendJSON(c)
//		}
//	}
//
//	var qWord []word
//	err = db.PublicDB().Model(&qWord).Where("? = any (words.kanjis)", form.Kanji).Select()
//	if db.IsNoRows(err, c.Path()) || len(qWord) == 0 {
//		qWord = make([]word, 0)
//	}
//	form.ListWord = qWord
//
//	var qSentence []sentence
//	err = db.PublicDB().Model(&qSentence).Where("? = any (sentences.kanjis)", form.Kanji).Select()
//	if db.IsNoRows(err, c.Path()) || len(qSentence) == 0 {
//		qSentence = make([]sentence, 0)
//	}
//	form.ListSentence = qSentence
//
//	return response.Success("Success get kanji", form).SendJSON(c)
//}

// @Summary Update kanji
// @Tags Kanji
// @Accept json
// @Produce json
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
// @Param req body kanjiUpdateReq true "json request body"
// @Success 200 {object} response.SuccessResponse{payload=kanji} "json with success = true"
// @Failure 400 {object} response.ErrorResponse "json with error = true"
// @Router /kanji/update [put]
func (h Kanji) Update(c echo.Context) error {
	var err error
	loginUser, err := getUserLoginInfo(c)
	if err != nil {
		return err
	}

	now := time.Now()
	req := new(kanjiUpdateReq)
	if err = c.Bind(req); err != nil {
		return err
	}
	if err = c.Validate(req); err != nil {
		return response.Error("Validation Failed", response.ValidationError(err)).SendJSON(c)
	}
	if req.KanjiID == 0 {
		return response.Error("ID Required", response.Payload{}).SendJSON(c)
	}

	qKanji := model.PublicKanji{
		KanjiID: req.KanjiID,
	}
	err = db.PublicDB().Model(&qKanji).WherePK().Select()
	if db.IsNoRows(err, c.Path()) {
		return response.Error("Not Found", response.Payload{}).SendJSON(c)
	}

	// begin Transaction Public
	publicTransaction := db.PublicBegin(loginUser.UserID, &now, c.Path())
	defer db.DeferHandleTransaction(publicTransaction)

	qKanji.Kanji = req.Kanji
	qKanji.Grade = req.Grade
	qKanji.StrokeCount = req.StrokeCount
	qKanji.Meanings = req.Meanings
	qKanji.KunReadings = req.KunReadings
	qKanji.OnReadings = req.OnReadings
	qKanji.NameReadings = req.NameReadings
	qKanji.Jlpt = req.Jlpt
	qKanji.Unicode = req.Unicode
	qKanji.HeisigEn = req.HeisigEn

	publicTransaction.Update(&qKanji)
	publicTransaction.Commit()

	kanji := kanji{
		KanjiID:      qKanji.KanjiID,
		Kanji:        qKanji.Kanji,
		Grade:        qKanji.Grade,
		StrokeCount:  qKanji.StrokeCount,
		Meanings:     qKanji.Meanings,
		KunReadings:  qKanji.KunReadings,
		OnReadings:   qKanji.OnReadings,
		NameReadings: qKanji.NameReadings,
		Jlpt:         qKanji.Jlpt,
		Unicode:      qKanji.Unicode,
		HeisigEn:     qKanji.HeisigEn,
	}

	return response.Success("Kanji Edited", kanji).SendJSON(c)
}

// @Summary Delete kanji
// @Tags Kanji
// @Accept json
// @Produce json
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
// @Param kanji_id path string true "kanji id" default(0)
// @Success 200 {object} response.SuccessResponse "json with success = true"
// @Failure 400 {object} response.ErrorResponse "json with error = true"
// @Router /kanji/{kanji_id} [delete]
func (h Kanji) Delete(c echo.Context) error {
	var err error
	now := time.Now()
	loginUser, err := getUserLoginInfo(c)
	if err != nil {
		return err
	}
	reqID, err := strconv.ParseInt(c.Param("kanji_id"), 10, 64)
	if err != nil {
		return err
	}

	var form model.PublicKanji
	form.KanjiID = reqID
	if form.KanjiID == 0 {
		return response.Error("ID required", response.Payload{}).SendJSON(c)
	} else {
		err = db.PublicDB().Model(&form).WherePK().Select()
		if err != nil {
			return response.Error("Not Found", response.Payload{}).SendJSON(c)
		}
	}

	publicTransaction := db.PublicBegin(loginUser.UserID, &now, c.Path())
	defer db.DeferHandleTransaction(publicTransaction)

	publicTransaction.Delete(&form)
	publicTransaction.Commit()

	return response.Success("Kanji Deleted", response.Payload{}).SendJSON(c)
}

// @Summary Kanji List
// @Tags Kanji
// @Accept json
// @Produce json
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
// @Param req body kanjiListReq false "json request body"
// @Success 200 {object} response.SuccessResponse{payload=[]kanji} "json with success = true"
// @Failure 400 {object} response.ErrorResponse "json with error = true"
// @Router /kanji/list [post]
func (h Kanji) List(c echo.Context) error {
	var err error
	loginUser, err := getUserLoginInfo(c)
	if err != nil {
		return err
	}

	req := new(kanjiListReq)
	if err = c.Bind(req); err != nil {
		return err
	}
	qList := getListKanji(req, loginUser, c.Path())

	return response.Success("Get List Kanji Success", qList).SendJSON(c)
}

func getListKanji(req *kanjiListReq, loginUser UserLogin, nameSpace string) []kanji {
	var qList []kanji
	var err error

	q := db.PublicDB().Model(&qList)
	sql.Search.StringLike(q, "kanjis.kanji", req.Kanji)
	sql.Search.StringEq(q, "kanjis.unicode", req.Unicode)
	sql.Search.StringEq(q, "kanjis.heisig_en", req.HeisigEn)

	err = q.Select()
	if db.IsNoRows(err, nameSpace) || len(qList) == 0 {
		qList = make([]kanji, 0)
	}

	return qList
}

//type testReq struct {
//	Word string `json:"word" example:""`
//}
//
//// @Summary Kanji test
//// @Tags Kanji
//// @Accept json
//// @Produce json
//// @securityDefinitions.apikey ApiKeyAuth
//// @in header
//// @name Authorization
//// @Param req body testReq false "json request body"
//// @Success 200 {object} response.SuccessResponse "json with success = true"
//// @Failure 400 {object} response.ErrorResponse "json with error = true"
//// @Router /kanji/test [post]
//func (h Kanji) Test(c echo.Context) error {
//	var err error
//	var found bool
//	req := new(testReq)
//	if err = c.Bind(req); err != nil {
//		return err
//	}
//
//	wordkanji := helper.GetKanji(req.Word)
//	kanjis := []string{}
//	for i := 0; i < len([]rune(wordkanji)); i++ {
//		k := string([]rune(wordkanji)[i])
//		found = false
//		for _, kanji := range kanjis {
//			if kanji == k {
//				found = true
//				break
//			}
//		}
//		if !found {
//			kanjis = append(kanjis, k)
//		}
//	}
//
//	for _, kanji := range kanjis {
//		if err = Validate.ValidateVar(kanji, "notexists=kanji"); err == nil {
//			newKanji := model.PublicKanji{}
//			err = newKanji.FetchKanji(kanji)
//		}
//	}
//
//	return response.Success("Success", response.Payload{
//
//	}).SendJSON(c)
//}

//func (k kanjiFetch) fetchKanji(kanji string) error {
//	var err error
//	url := "https://kanjiapi.dev/v1/kanji/" + kanji
//	spaceClient := http.Client{
//		Timeout: time.Second * 10, // Timeout after 2 seconds
//	}
//	req, err := http.NewRequest(http.MethodGet, url, nil)
//	if err != nil {
//		return err
//	}
//	req.Header.Set("Accept", "application/json")
//	res, err := spaceClient.Do(req)
//	if err != nil {
//		return err
//	}
//	if res.Body != nil {
//		defer res.Body.Close()
//	}
//	body, err := ioutil.ReadAll(res.Body)
//	if err != nil {
//		return err
//	}
//
//	err = json.Unmarshal(body, &m)
//	if err != nil {
//		return err
//	}
//	return nil
//}

//func fetchKanji(kanji string) error {
//	var err error
//	url := "https://kanjiapi.dev/v1/kanji/" + kanji
//	spaceClient := http.Client{
//		Timeout: time.Second * 10, // Timeout after 2 seconds
//	}
//	req, err := http.NewRequest(http.MethodGet, url, nil)
//	if err != nil {
//		return err
//	}
//	req.Header.Set("Accept", "application/json")
//	res, err := spaceClient.Do(req)
//	if err != nil {
//		return err
//	}
//	if res.Body != nil {
//		defer res.Body.Close()
//	}
//	body, err := ioutil.ReadAll(res.Body)
//	if err != nil {
//		return err
//	}
//
//	newKanji := model.PublicKanji{}
//	err = json.Unmarshal(body, &newKanji)
//	if err != nil {
//		return err
//	}
//	return nil
//}
