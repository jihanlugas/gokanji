package controller

import (
	"github.com/go-pg/pg/v10"
	"github.com/labstack/echo/v4"
	"gokanji/db"
	"gokanji/helper"
	"gokanji/model"
	"gokanji/response"
	"gokanji/sql"
	"strconv"
	"time"
)

type Word struct{}

func WordComposer() Word {
	return Word{}
}

type wordListReq struct {
	Word        string `json:"word" example:" "`
	Kana        string `json:"kana" example:" "`
	Description string `json:"description" example:" "`
}

type word struct {
	tableName struct{} `pg:"public.words,alias:words,,discard_unknown_columns"`

	WordID      int64    `pg:"word_id,pk" json:"wordId"`
	Word        string   `pg:"word,use_zero" json:"word" example:" "`
	Kana        string   `pg:"kana,use_zero" json:"kana" example:" "`
	Means       []string `pg:"means,array" json:"means" example:" "`
	Kanjis      []string `pg:"kanjis,array" json:"kanjis" example:" "`
	Description string   `pg:"description" json:"description" example:" "`
}

type wordCreateReq struct {
	Word        string   `json:"word" example:" " validate:"required"`
	Kana        string   `json:"kana" example:" " validate:"required"`
	Means       []string `json:"means" example:" " validate:"required"`
	Description string   `json:"description"`
}

type wordUpdateReq struct {
	WordID      int64    `json:"wordId" validate:"required"`
	Word        string   `json:"word" example:" " validate:"required"`
	Kana        string   `json:"kana" example:" " validate:"required"`
	Means       []string `json:"means" example:" " validate:"required"`
	Description string   `json:"description"`
}

// @Summary Word Create
// @Tags Word
// @Accept json
// @Produce json
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
// @Param req body wordCreateReq false "json request body"
// @Success 200 {object} response.SuccessResponse{payload=word} "json with success = true"
// @Failure 400 {object} response.ErrorResponse "json with error = true"
// @Router /word/form [post]
func (h Word) Create(c echo.Context) error {
	var err error
	var found bool
	loginUser, err := getUserLoginInfo(c)
	if err != nil {
		return err
	}
	now := time.Now()
	req := new(wordCreateReq)
	if err = c.Bind(req); err != nil {
		return err
	}
	if err = Validate.ValidateVar(req.Word, "notexists=word"); err != nil {
		return response.Error("Validation Failed", response.ValidationError(err)).SendJSON(c)
	}

	// begin Transaction Public
	publicTransaction := db.PublicBegin(loginUser.UserID, &now, c.Path())
	defer db.DeferHandleTransaction(publicTransaction)

	wordkanji := helper.GetKanji(req.Word)
	kanjis := []string{}
	for i := 0; i < len([]rune(wordkanji)); i++ {
		k := string([]rune(wordkanji)[i])
		found = false
		for _, kanji := range kanjis {
			if kanji == k {
				found = true
				break
			}
		}
		if !found {
			kanjis = append(kanjis, k)
		}
	}

	var listKanji []model.PublicKanji
	err = db.PublicDB().Model(&listKanji).Where("kanjis.kanji IN (?) ", pg.In(kanjis)).Select()
	if db.IsNoRows(err, c.Path()) || len(listKanji) == 0 {
		listKanji = make([]model.PublicKanji, 0)
	}

	for _, newkanji := range kanjis {
		kanji := model.PublicKanji{}
		userkanji := model.PublicUserkanji{}
		kanjiexist := false
		for _, list := range listKanji {
			if list.Kanji == newkanji {
				kanjiexist = true
				kanji = list
				break
			}
		}

		if !kanjiexist {
			if err = kanji.FetchKanji(newkanji); err == nil {
				publicTransaction.Insert(&kanji)
			}
			userkanji.UserID = loginUser.UserID
			userkanji.KanjiID = kanji.KanjiID
			publicTransaction.Insert(&userkanji)
		} else {
			err = db.PublicDB().Model(&userkanji).
				Where("userkanjis.user_id = ? ", loginUser.UserID).
				Where("userkanjis.kanji_id = ? ", kanji.KanjiID).
				First()
			if db.IsNoRows(err, c.Path()) {
				userkanji.UserID = loginUser.UserID
				userkanji.KanjiID = kanji.KanjiID
				publicTransaction.Insert(&userkanji)
			}
		}
	}

	newWord := model.PublicWord{
		Word:   req.Word,
		Kana:   req.Kana,
		Means:  req.Means,
		Kanjis: kanjis,
		Description: req.Description,
		UserID: loginUser.UserID,
	}
	publicTransaction.Insert(&newWord)
	publicTransaction.Commit()

	word := word{
		WordID: newWord.WordID,
		Word:   newWord.Word,
		Kana:   newWord.Kana,
		Means:  newWord.Means,
		Kanjis: kanjis,
		Description: newWord.Description,
	}

	return response.Success("Word Created", word).SendJSON(c)
}

// @Summary Get word
// @Tags Word
// @Accept json
// @Produce json
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
// @Param word_id path string true "word id" default(0)
// @Success 200 {object} response.SuccessResponse{payload=word} "json with success = true"
// @Failure 400 {object} response.ErrorResponse "json with error = true"
// @Router /word/{word_id} [get]
func (h Word) Get(c echo.Context) error {
	var err error
	loginUser, err := getUserLoginInfo(c)
	if err != nil {
		return err
	}
	reqID, err := strconv.ParseInt(c.Param("word_id"), 10, 64)
	if err != nil {
		return err
	}

	var form word
	form.WordID = reqID
	if form.WordID == 0 {
		return response.Error("ID required", response.Payload{}).SendJSON(c)
	} else {
		err = db.PublicDB().Model(&form).WherePK().Where("words.user_id = ? ", loginUser.UserID).Select()
		if err != nil {
			return response.Error("Not Found", response.Payload{}).SendJSON(c)
		}
	}
	return response.Success("Success get word", form).SendJSON(c)
}

// @Summary Update word
// @Tags Word
// @Accept json
// @Produce json
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
// @Param req body wordUpdateReq true "json request body"
// @Success 200 {object} response.SuccessResponse{payload=word} "json with success = true"
// @Failure 400 {object} response.ErrorResponse "json with error = true"
// @Router /word/update [put]
func (h Word) Update(c echo.Context) error {
	var err error
	var found bool
	loginUser, err := getUserLoginInfo(c)
	if err != nil {
		return err
	}
	now := time.Now()
	req := new(wordUpdateReq)
	if err = c.Bind(req); err != nil {
		return err
	}
	if err = c.Validate(req); err != nil {
		return response.Error("Validation Failed", response.ValidationError(err)).SendJSON(c)
	}
	if req.WordID == 0 {
		return response.Error("ID Required", response.Payload{}).SendJSON(c)
	}

	qWord := model.PublicWord{WordID: req.WordID}
	err = db.PublicDB().Model(&qWord).WherePK().Select()
	if db.IsNoRows(err, c.Path()) {
		return response.Error("Not Found", response.Payload{}).SendJSON(c)
	}

	publicTransaction := db.PublicBegin(loginUser.UserID, &now, c.Path())
	defer db.DeferHandleTransaction(publicTransaction)

	wordkanji := helper.GetKanji(req.Word)
	kanjis := []string{}
	for i := 0; i < len([]rune(wordkanji)); i++ {
		k := string([]rune(wordkanji)[i])
		found = false
		for _, kanji := range kanjis {
			if kanji == k {
				found = true
				break
			}
		}
		if !found {
			kanjis = append(kanjis, k)
		}
	}

	var listKanji []model.PublicKanji
	err = db.PublicDB().Model(&listKanji).Where("kanjis.kanji IN (?) ", pg.In(kanjis)).Select()
	if db.IsNoRows(err, c.Path()) || len(listKanji) == 0 {
		listKanji = make([]model.PublicKanji, 0)
	}

	for _, newkanji := range kanjis {
		kanji := model.PublicKanji{}
		userkanji := model.PublicUserkanji{}
		kanjiexist := false
		for _, list := range listKanji {
			if list.Kanji == newkanji {
				kanjiexist = true
				kanji = list
				break
			}
		}

		if !kanjiexist {
			if err = kanji.FetchKanji(newkanji); err == nil {
				publicTransaction.Insert(&kanji)
			}
			userkanji.UserID = loginUser.UserID
			userkanji.KanjiID = kanji.KanjiID
			publicTransaction.Insert(&userkanji)
		} else {
			err = db.PublicDB().Model(&userkanji).
				Where("userkanjis.user_id = ? ", loginUser.UserID).
				Where("userkanjis.kanji_id = ? ", kanji.KanjiID).
				First()
			if db.IsNoRows(err, c.Path()) {
				userkanji.UserID = loginUser.UserID
				userkanji.KanjiID = kanji.KanjiID
				publicTransaction.Insert(&userkanji)
			}
		}
	}

	//for _, kanji := range kanjis {
	//	if err = Validate.ValidateVar(kanji, "notexists=kanji"); err == nil {
	//		newKanji := model.PublicKanji{}
	//		if err = newKanji.FetchKanji(kanji); err == nil {
	//			publicTransaction.Insert(&newKanji)
	//		}
	//	}
	//}

	qWord.Word = req.Word
	qWord.Kana = req.Kana
	qWord.Means = req.Means
	qWord.Kanjis = kanjis
	qWord.Description = req.Description
	qWord.UserID = loginUser.UserID

	publicTransaction.Update(&qWord)
	publicTransaction.Commit()

	word := word{
		WordID: qWord.WordID,
		Word:   qWord.Word,
		Kana:   qWord.Kana,
		Means:  qWord.Means,
		Kanjis: qWord.Kanjis,
		Description: qWord.Description,
	}
	return response.Success("Word Edited", word).SendJSON(c)
}

// @Summary Delete word
// @Tags Word
// @Accept json
// @Produce json
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
// @Param word_id path string true "word id" default(0)
// @Success 200 {object} response.SuccessResponse "json with success = true"
// @Failure 400 {object} response.ErrorResponse "json with error = true"
// @Router /word/{word_id} [delete]
func (h Word) Delete(c echo.Context) error {
	var err error
	now := time.Now()
	loginUser, err := getUserLoginInfo(c)
	if err != nil {
		return err
	}
	reqID, err := strconv.ParseInt(c.Param("word_id"), 10, 64)
	if err != nil {
		return err
	}

	var form model.PublicWord
	form.WordID = reqID
	if form.WordID == 0 {
		return response.Error("ID required", response.Payload{}).SendJSON(c)
	} else {
		err = db.PublicDB().Model(&form).WherePK().Select()
		if err != nil {
			return response.Error("Not Found", response.Payload{}).SendJSON(c)
		}
	}

	publicTransaction := db.PublicBegin(loginUser.UserID, &now, c.Path())
	defer db.DeferHandleTransaction(publicTransaction)

	publicTransaction.Delete(&form)
	publicTransaction.Commit()

	return response.Success("Word Deleted", response.Payload{}).SendJSON(c)
}

// @Summary Word List
// @Tags Word
// @Accept json
// @Produce json
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
// @Param req body wordListReq false "json request body"
// @Success 200 {object} response.SuccessResponse{payload=[]word} "json with success = true"
// @Failure 400 {object} response.ErrorResponse "json with error = true"
// @Router /word/list [post]
func (h Word) List(c echo.Context) error {
	var err error
	loginUser, err := getUserLoginInfo(c)
	if err != nil {
		return err
	}

	req := new(wordListReq)
	if err = c.Bind(req); err != nil {
		return err
	}
	qList := getListWord(req, loginUser, c.Path())

	return response.Success("Get List Word Success", qList).SendJSON(c)
}

func getListWord(req *wordListReq, loginUser UserLogin, nameSpace string) []word {
	var qList []word
	var err error

	q := db.PublicDB().Model(&qList).Where("words.user_id = ? ", loginUser.UserID)
	sql.Search.StringLike(q, "words.word", req.Word)
	sql.Search.StringLike(q, "words.kana", req.Kana)
	sql.Search.StringLike(q, "words.description", req.Description)
	err = q.Select()
	if db.IsNoRows(err, nameSpace) || len(qList) == 0 {
		qList = make([]word, 0)
	}
	return qList
}
