package controller

import (
	"encoding/binary"
	"gokanji/constant"
	"gokanji/cryption"
	"gokanji/response"
	"gokanji/validator"
	"io/ioutil"
	"time"

	jsoniter "github.com/json-iterator/go"
	"github.com/labstack/echo/v4"
)

var json jsoniter.API
var Validate *validator.CustomValidator

func init() {
	json = jsoniter.ConfigFastest
	Validate = validator.NewValidator()
}

type UserLogin struct {
	UserID int64
}

func Ping(c echo.Context) error {
	return response.Success("Hallo　世界", response.Payload{}).SendJSON(c)
}

type Director struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

type Star struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

type Writer struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

type Similar struct {
	ID         string `json:"id"`
	Title      string `json:"title"`
	FullTitle  string `json:"fullTitle"`
	Year       string `json:"year"`
	Image      string `json:"image"`
	Plot       string `json:"plot"`
	Directors  string `json:"directors"`
	Stars      string `json:"stars"`
	ImDbRating string `json:"imDbRating"`
}

type Actor struct {
	ID          string `json:"id"`
	Image       string `json:"image"`
	Name        string `json:"name"`
	AsCharacter string `json:"as"`
}

type Item struct {
	ID              string     `json:"id"`
	Title           string     `json:"title"`
	OriginalTitle   string     `json:"originalTitle"`
	FullTitle       string     `json:"fullTitle"`
	Type            string     `json:"type"`
	Year            string     `json:"year"`
	Image           string     `json:"image"`
	ReleaseDate     string     `json:"releaseDate"`
	RuntimeMins     string     `json:"runtimeMins"`
	RuntimeStr      string     `json:"runtimeStr"`
	Plot            string     `json:"plot"`
	PlotLocal       string     `json:"plotLocal"`
	PlotLocalIsRtl  bool       `json:"plotLocalIsRtl"`
	Awards          string     `json:"awards"`
	Directors       string     `json:"directors"`
	DirectorList    []Director `json:"directorList"`
	Writers         string     `json:"writters"`
	WriterList      []Writer   `json:"writterList"`
	Stars           string     `json:"stars"`
	StarList        []Star     `json:"starList"`
	ImDbRating      string     `json:"imDbRating"`
	ImDbRatingVotes string     `json:"imDbRatingVotes"`
	Crew            string     `json:"crew"`
	Similar         []Similar  `json:"similarList"`
	Genres          string     `json:"genres"`
	ActorList       []Actor    `json:"actorList"`
}

func Movie(c echo.Context) error {
	var err error
	file, err := ioutil.ReadFile("json/movie.json")
	if err != nil {
		return response.Error("Err "+err.Error(), err).SendJSON(c)
	}

	var data []Item

	err = json.Unmarshal([]byte(file), &data)
	if err != nil {
		return response.Error("Err "+err.Error(), err).SendJSON(c)
	}

	return response.Success("Success", data).SendJSON(c)
}

func getLoginToken(userID int64, expiredAt time.Time) ([]byte, error) {
	expiredUnix := expiredAt.Unix()

	tokenPayload := make([]byte, constant.TokenPayloadLen)
	binary.BigEndian.PutUint64(tokenPayload, uint64(expiredUnix)) // Expired date
	binary.BigEndian.PutUint64(tokenPayload[28:], uint64(userID))

	return cryption.EncryptAES64(tokenPayload)
}

func getUserLoginInfo(c echo.Context) (UserLogin, error) {
	if u, ok := c.Get(constant.TokenUserContext).(UserLogin); ok {
		return u, nil
	} else {
		return UserLogin{}, response.ErrorForce("Akses tidak diterima", response.Payload{})
	}
}
