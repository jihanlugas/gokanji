package controller

import (
	"github.com/labstack/echo/v4"
	"gokanji/db"
	"gokanji/response"
	"gokanji/sql"
)

type Userkanji struct{}

func UserkanjiComposer() Userkanji {
	return Userkanji{}
}

func (h Userkanji) Create(c echo.Context) error {
	return response.Success("Success get kanji", response.Payload{}).SendJSON(c)
}

func (h Userkanji) Get(c echo.Context) error {
	return response.Success("Success get kanji", response.Payload{}).SendJSON(c)
}

type kanjiSearchReq struct {
	Kanji        string   `pg:"kanji,use_zero" json:"kanji" example:"" validate:"required"`
}

// @Summary Search kanji
// @Tags Userkanji
// @Accept json
// @Produce json
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
// @Param req body kanjiSearchReq false "json request body"
// @Success 200 {object} response.SuccessResponse{payload=kanji} "json with success = true"
// @Failure 400 {object} response.ErrorResponse "json with error = true"
// @Router /userkanji/search [post]
func (h Userkanji) Search(c echo.Context) error {
	var err error
	loginUser, err := getUserLoginInfo(c)
	if err != nil {
		return err
	}

	req := new(kanjiSearchReq)
	if err = c.Bind(req); err != nil {
		return err
	}
	if err = c.Validate(req); err != nil {
		return response.Error("Validation Failed", response.ValidationError(err)).SendJSON(c)
	}

	var form kanji
	form.Kanji = req.Kanji
	if form.Kanji == "" {
		return response.Error("Kanji required", response.Payload{}).SendJSON(c)
	} else {
		err = db.PublicDB().Model(&form).
			Join("Join public.userkanjis userkanjis ON kanjis.kanji_id = userkanjis.kanji_id").
			Where("userkanjis.user_id = ? ", loginUser.UserID).
			Where("kanjis.kanji = ? ", form.Kanji).First()
		if db.IsNoRows(err, c.Path()) {
			return response.Error("Not Found", response.Payload{}).SendJSON(c)
		}
	}

	var qWord []word
	err = db.PublicDB().Model(&qWord).Where("? = any (words.kanjis)", form.Kanji).Select()
	if db.IsNoRows(err, c.Path()) || len(qWord) == 0 {
		qWord = make([]word, 0)
	}
	form.ListWord = qWord

	var qSentence []sentence
	err = db.PublicDB().Model(&qSentence).Where("? = any (sentences.kanjis)", form.Kanji).Select()
	if db.IsNoRows(err, c.Path()) || len(qSentence) == 0 {
		qSentence = make([]sentence, 0)
	}
	form.ListSentence = qSentence

	return response.Success("Success get kanji", form).SendJSON(c)
}

func (h Userkanji) Update(c echo.Context) error {
	return response.Success("Success get kanji", response.Payload{}).SendJSON(c)
}

func (h Userkanji) Delete(c echo.Context) error {
	return response.Success("Success get kanji", response.Payload{}).SendJSON(c)
}

// @Summary Userkanji List
// @Tags Userkanji
// @Accept json
// @Produce json
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
// @Param req body kanjiListReq false "json request body"
// @Success 200 {object} response.SuccessResponse{payload=[]kanji} "json with success = true"
// @Failure 400 {object} response.ErrorResponse "json with error = true"
// @Router /userkanji/list [post]
func (h Userkanji) List(c echo.Context) error {
	var err error
	loginUser, err := getUserLoginInfo(c)
	if err != nil {
		return err
	}

	req := new(kanjiListReq)
	if err = c.Bind(req); err != nil {
		return err
	}
	qList := getListUserkanji(req, loginUser, c.Path())

	return response.Success("Get List Kanji Success", qList).SendJSON(c)
}

func getListUserkanji(req *kanjiListReq, loginUser UserLogin, nameSpace string) []kanji {
	var qList []kanji
	var err error

	q := db.PublicDB().Model(&qList).
		Join("Join public.userkanjis userkanjis ON kanjis.kanji_id = userkanjis.kanji_id").
		Where("userkanjis.user_id = ? ", loginUser.UserID)
	sql.Search.StringLike(q, "kanjis.kanji", req.Kanji)
	sql.Search.StringEq(q, "kanjis.unicode", req.Unicode)
	sql.Search.StringEq(q, "kanjis.heisig_en", req.HeisigEn)

	err = q.Select()
	if db.IsNoRows(err, nameSpace) || len(qList) == 0 {
		qList = make([]kanji, 0)
	}

	return qList
}
