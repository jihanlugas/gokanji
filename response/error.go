package response

import (
	"github.com/go-playground/validator/v10"
	"gokanji/config"
	"strconv"

)

const (
	ErrorUsernameTaken       = "error_username_taken"
	ErrorSelectionInvalid    = "error_selection_invalid"
	ErrorLengthTooBig        = "error_length_too_big"
	ErrorLengthTooShort      = "error_length_too_short"
	ErrorInvalidEmail        = "error_invalid_email"
	ErrorFieldRequired       = "error_field_required"
	ErrorUserOrPassInvalid   = "error_user_pass_invalid"
	ErrorUserNotActive       = "error_user_not_active"
	ErrorLoginFailed         = "error_login_failed"
	ErrorRegistrationFailed  = "error_registration_failed"
	ErrorNotAuthorized       = "error_not_authorized"
	ErrorUnresolved          = "error_unresolved"
	ErrorFixedLength         = "error_fix_length"
	ErrorInvalidDateTime     = "error_invalid_datetime"
	ErrorDataNotFound        = "error_data_not_found"
	ErrorDataMasterNotExists = "error_data_master_not_exists"
	ErrorCodeTaken           = "error_code_taken"
	ErrorFlexible            = "error_flexible"
	ErrorSelectionMustUnique = "error_selection_must_unique"
	ErrorPhotoUpload         = "error_photo_upload"
	ErrorAlreadyExists       = "error_already_exists"
)

type FieldError struct {
	Field string `json:"field"`
	Msg   string `json:"msg"`
}

func getFieldError(str ...string) FieldError {
	switch str[1] {
	case ErrorLengthTooBig:
		return FieldError{
			Field: str[0],
			Msg:   "maksimum " + str[2],
		}
	case ErrorLengthTooShort:
		return FieldError{
			Field: str[0],
			Msg:   "minimum " + str[2],
		}
	case ErrorFlexible:
		return FieldError{
			Field: str[0],
			Msg:   str[2],
		}
	case ErrorAlreadyExists:
		return FieldError{
			Field: str[0],
			Msg:   "Sudah pernah terdaftar",
		}
	default:
		return FieldError{
			Field: str[0],
			Msg:   "",
		}
	}
}

func getListError(err error) Payload {
	listError := Payload{}
	fieldsError := err.(validator.ValidationErrors)

	for _, fieldError := range fieldsError {
		switch fieldError.ActualTag() {
		case "notexists":
			listError[fieldError.Field()] = getFieldError(fieldError.Field(), ErrorAlreadyExists)
		case "oneof", "exists", "weekday":
			listError[fieldError.Field()] = getFieldError(fieldError.Field(), ErrorSelectionInvalid)
		case "lte", "max":
			listError[fieldError.Field()] = getFieldError(fieldError.Field(), ErrorLengthTooBig, fieldError.Param())
		case "gte", "min":
			listError[fieldError.Field()] = getFieldError(fieldError.Field(), ErrorLengthTooShort, fieldError.Param())
		case "email":
			listError[fieldError.Field()] = getFieldError(fieldError.Field(), ErrorInvalidEmail)
		case "required", "required_with":
			listError[fieldError.Field()] = getFieldError(fieldError.Field(), ErrorFieldRequired)
		case "len":
			listError[fieldError.Field()] = getFieldError(fieldError.Field(), ErrorFixedLength, fieldError.Param())
		case "datetime":
			listError[fieldError.Field()] = getFieldError(fieldError.Field(), ErrorInvalidDateTime)
		case "unique":
			listError[fieldError.Field()] = getFieldError(fieldError.Field(), ErrorSelectionMustUnique)
		case "pagelimit":
			listError[fieldError.Field()] = getFieldError(fieldError.Field(), ErrorLengthTooBig, strconv.Itoa(config.DataPerPage))
		case "photoCheck":
			listError[fieldError.Field()] = getFieldError(fieldError.Field(), ErrorPhotoUpload)
		}
	}

	return listError
}
