package sql

import (
	"github.com/go-pg/pg/v10"
	"github.com/go-pg/pg/v10/orm"
	jsoniter "github.com/json-iterator/go"
	"strconv"
	"strings"
	"time"
)

var json jsoniter.API
var Search *search

type search struct{}

func init() {
	json = jsoniter.ConfigFastest
	Search = &search{}
}

func (s *search) Int64In(sql *orm.Query, column string, ids []int64) {
	if len(ids) > 0 {
		sql.Where(column+" in (?)", pg.In(ids))
	}
}

func (s *search) IntIn(sql *orm.Query, column string, ids []int) {
	if len(ids) > 0 {
		sql.Where(column+" in (?)", pg.In(ids))
	}
}

func (s *search) DateTimeBetween(sql *orm.Query, column string, from, to *time.Time) {
	if from != nil && to != nil {
		sql.Where(column+" BETWEEN ? AND ?", *from, *to)
	} else if from != nil {
		sql.Where(column+" >= ?", *from)
	} else if to != nil {
		sql.Where(column+" <= ?", *to)
	}
}

func (s *search) DateTime(sql *orm.Query, column, operator string, val *time.Time) {
	if val != nil {
		sql.Where(column+" "+operator+" ?", *val)
	}
}

func (s *search) IntBetween(sql *orm.Query, column, from, to string) {
	var iFrom *int
	var iTo *int
	from = strings.TrimSpace(from)
	to = strings.TrimSpace(to)

	if v, err := strconv.Atoi(from); err == nil {
		iFrom = &v
	}
	if v, err := strconv.Atoi(to); err == nil {
		iTo = &v
	}

	if iFrom != nil && iTo != nil {
		sql.Where(column+" BETWEEN ? AND ?", *iFrom, *iTo)
	} else if iFrom != nil {
		sql.Where(column+" >= ?", *iFrom)
	} else if iTo != nil {
		sql.Where(column+" <= ?", *iTo)
	}
}

func (s *search) Int(sql *orm.Query, column, operator, val string) {
	var iVal *int
	val = strings.TrimSpace(val)

	if v, err := strconv.Atoi(val); err == nil {
		iVal = &v
	}

	if iVal != nil {
		sql.Where(column+" "+operator+" ?", *iVal)
	}
}

func (s *search) Int64Between(sql *orm.Query, column, from, to string) {
	var iFrom *int64
	var iTo *int64
	from = strings.TrimSpace(from)
	to = strings.TrimSpace(to)

	if v, err := strconv.ParseInt(from, 10, 64); err == nil {
		iFrom = &v
	}
	if v, err := strconv.ParseInt(to, 10, 64); err == nil {
		iTo = &v
	}

	if iFrom != nil && iTo != nil {
		sql.Where(column+" BETWEEN ? AND ?", *iFrom, *iTo)
	} else if iFrom != nil {
		sql.Where(column+" >= ?", *iFrom)
	} else if iTo != nil {
		sql.Where(column+" <= ?", *iTo)
	}
}

func (s *search) Int64(sql *orm.Query, column, operator, val string) {
	var iVal *int64
	val = strings.TrimSpace(val)

	if v, err := strconv.ParseInt(val, 10, 64); err == nil {
		iVal = &v
	}

	if iVal != nil {
		sql.Where(column+" "+operator+" ?", *iVal)
	}
}

func (s *search) StringLike(sql *orm.Query, column, strVal string) {
	strVal = strings.TrimSpace(strVal)
	if strVal != "" {
		sql.Where("lower("+column+") LIKE ?", "%"+strings.ToLower(strVal)+"%")
	}
}

func (s *search) StringEq(sql *orm.Query, column, strVal string) {
	strVal = strings.TrimSpace(strVal)
	if strVal != "" {
		sql.Where(column+" = ?", strVal)
	}
}

func (s *search) Bool(sql *orm.Query, column, strVal string) {
	strVal = strings.TrimSpace(strVal)
	switch strVal {
	case "1":
		sql.Where(column+" = ?", true)
	case "0":
		sql.Where(column+" = ?", false)
	}
}